<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsModel extends Model
{
  protected $fillable = [
    'title', 'body', 'image_src'
  ];
}
