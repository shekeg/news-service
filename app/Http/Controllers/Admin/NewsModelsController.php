<?php

namespace App\Http\Controllers\Admin;

use App\NewsModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Http\UploadedFile;

class NewsModelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $news = NewsModel::all();
      return view('admin.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.news.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'title' => 'required|string|max:255',
        'body' => 'required',
        'image_src' => 'required|image',
      ]);
      $image = $request->file('image_src');
      $new_name = date('Y-m-d-H-i-s') . '.' . $image->getClientOriginalExtension();
      $image->move(public_path() . '\img', $new_name);

      NewsModel::create([
        'title' => $request['title'],
        'body' => $request['body'],
        'image_src' => $new_name,
      ]);
      return redirect()->route('admin.newsModels.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NewsModel  $newsModel
     * @return \Illuminate\Http\Response
     */
    public function show(NewsModel $newsModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NewsModel  $newsModel
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, NewsModel $newsModel)
    {
      $entity = $newsModel;
      return view('admin.news.form', compact('entity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NewsModel  $newsModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NewsModel $newsModel)
    {
      $this->validate($request, [
        'title' => 'required|string|max:255',
        'image_src' => 'image',
      ]);
      
      $new_name = $newsModel['image_src'];

      if (!empty($request['image_src'])) {
        $image = $request->file('image_src');
        $new_name = date('Y-m-d-H-i-s') . '.' . $image->getClientOriginalExtension();
        $image->move(public_path() . '\img', $new_name);
      }

      $newsModel->update([
        'title' => $request['title'],
        'body' => $request['body'],
        'image_src' => $new_name,
      ]);
      return redirect()->route('admin.newsModels.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NewsModel  $newsModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(NewsModel $newsModel)
    {
      $newsModel->delete();
      return redirect()->route('admin.newsModels.index');
    }
}
