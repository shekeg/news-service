<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NewsModel;


class PagesController extends Controller
{
    public function index(Request $request)
    {
      $news = NewsModel::limit(10)->get();
      return view('index', ['news' => $news]);
    }

    public function show(NewsModel $NewsModel) {
      return view('show', ['news' => $NewsModel]);
    }

}
