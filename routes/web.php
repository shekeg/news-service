<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->name('index');
Route::get('news/{NewsModel}', 'PagesController@show')->name('news.show');

Auth::routes();

Route::group(['prefix'=>'admin', 'namespace'=>'Admin', 'middleware'=>['auth']], function() {
  Route::group(['as' => 'admin.'], function() {
    Route::get('home', 'HomeController@index')->name('home');
    Route::resource('newsModels', 'NewsModelsController');
  });
});