@extends('layouts.app')

@section('content')
  <div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
					<div class="card-title">News</div>
							<table class="table">
								<tr>
									<th>ID</th>
									<th>Title</th>
									<th>Body</th>
									<th>Image</th>
								</tr>
									<tr>
										<td>{{ $news->id }}</td>
										<td>{{ $news->title }}</td>
										<td>{{ $news->body }}</td>
                    <td>
                      <img class="table__image" src="\img\{{ $news->image_src }}" alt="">
                    </td>
									</tr>
							</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection