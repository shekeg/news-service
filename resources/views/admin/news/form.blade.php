@extends('admin.layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="card">
					<div class="card-body">
					@if(empty($entity))
						<div class="card-title">Create news</div>
					@else
						<div class="card-title">Edit news</div>
					@endif
						<form action="@if(empty($entity)){{ route('admin.newsModels.store') }}@else{{ route('admin.newsModels.update', $entity->id) }}@endif" method="POST" enctype="multipart/form-data">
							{{ csrf_field() }}
							@isset($entity)
								 {{ method_field('PUT') }}
							@endisset
								@include('admin.fields.text', ['type'=>'text', 'field' => 'title', 'name' => 'Title'])
                @include('admin.fields.textarea', ['field' => 'body', 'name' => 'Body', 'rows' => 10])
                @include('admin.fields.image', ['field' => 'image_src', 'name' => 'Image'])
							<input class="btn btn-primary" type="submit" value="Save">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection