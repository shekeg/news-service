@extends('admin.layouts.app')

@section('content')
  <div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
					<div class="card-title">News</div>
						@if($news->count() > 0)
							<table class="table">
								<tr>
									<th>ID</th>
									<th>Title</th>
									<th>Body</th>
									<th>Image</th>
									<th>Action</th>
								</tr>
								@foreach($news as $newsItem)
									<tr>
										<td>{{ $newsItem->id }}</td>
										<td>{{ $newsItem->title }}</td>
										<td>{{ $newsItem->body }}</td>
                    <td>
                      <img class="table__image" src="\img\{{ $newsItem->image_src }}" alt="">
                    </td>
										<td>
											<form action="{{ route('admin.newsModels.destroy', $newsItem->id) }}" method="POST">
												{{ csrf_field() }}
                        {{ method_field('delete') }}
												<a  type="button" class="btn btn-primary" href="{{ route('admin.newsModels.edit', $newsItem->id) }}">Change news</a>
												<button type="submit" class="btn btn-danger">Delete</button>
											</form>
										</td>
									</tr>
								@endforeach
							</table>
						@else
							No news
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection