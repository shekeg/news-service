@extends('admin.fields.main')

@section('field')
	<input type="{{ isset($type) ? $type : 'text' }}" name="{{ $field }}" value="{{ isset($isEmpty) ? '' : old($field, (isset($entity) ? $entity->$field : '')) }}" class="form-control">
@overwrite
