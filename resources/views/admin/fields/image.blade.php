@extends('admin.fields.main')

@section('field')
	@if(isset($entity->$field) && !empty($entity->$field))
		<img src="{{ '\img\\' . $entity->$field }}" class="img-thumbnail" width="304" height="236">
	@endif
	<div class="btn btn-default btn-file">
		{{ Form::file($field) }}
	</div>

@overwrite
